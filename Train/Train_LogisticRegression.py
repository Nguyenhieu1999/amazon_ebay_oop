import sys
import os
import constant

sys.path.append(os.path.abspath(constant.toSubLibpath()))

from Train import TrainBase
from DataPreProcess import train_test_split

from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression

class Training_LogisticRegression(TrainBase):
    #OVERRIDE
    def train_to_model(self, ratio=0.8, penalty='l2', *, dual=False, tol=0.0001, C=1.0, fit_intercept=True, intercept_scaling=1, class_weight=None, 
        random_state=None, solver='lbfgs', max_iter=100, multi_class='auto', verbose=0, warm_start=False, n_jobs=None, l1_ratio=None):

        self.ratio = ratio

        train_data, test_data, train_labels, test_labels = train_test_split(self.vectors, self.labels, int(self.size_of_data * ratio))

        model = LogisticRegression(
            penalty=penalty, dual=dual, tol=tol, C=C, fit_intercept=fit_intercept, intercept_scaling=intercept_scaling, class_weight=class_weight,
            random_state=random_state, solver=solver, max_iter=max_iter, multi_class=multi_class, verbose=verbose, warm_start=warm_start, n_jobs=n_jobs, l1_ratio=l1_ratio
        )

        model.fit(train_data, train_labels)

        self.accuracy = accuracy_score(model.predict(test_data), test_labels)
        self.model = model